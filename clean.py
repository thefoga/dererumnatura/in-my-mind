# coding: utf-8

import os

from src.common.io import get_config, get_folders
from src.common.user import get_yn_answer
from src.obsidian.attachments import find_all_attached, find_all_attachments, file2attach


config = get_config('./config.ini')


def main():
    root_f, _, root_attach = get_folders(config)

    all_attached = find_all_attached(root_f, root_attach)
    all_attachments = find_all_attachments(root_attach)

    print('found {:d} attached things'.format(len(all_attached)))
    print('found {:d} attachments in local folder'.format(len(all_attachments)))

    can_remove = False
    if get_yn_answer('feeling lucky and actually REMOVE FROM LOCAL DISK useless files?'):
        can_remove = True

    print('scanning useless attachments ...')
    for attachment in all_attachments:
        if file2attach(root_attach)(attachment) not in all_attached:
            print('  {} is not attached !'.format(attachment))

            if can_remove:
                try:
                    os.remove(attachment)
                    print('  ... trashed!')
                except:
                    print('  ... but cannot remove it!')

    print('scanning attached files but missing locally ...')
    all_attachments = list(map(file2attach(root_attach), all_attachments))
    for attached in all_attached:
        if attached not in all_attachments:
            print('  {} is attached but cannot be found in attachment folder !'.format(attached))


if __name__ == '__main__':
    main()
