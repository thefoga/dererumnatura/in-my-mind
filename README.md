# In my mind

from the legend Gigi D'Agostino here you cand find a set of tools to manage your [Obsidian](https://obsidian.md/)-based [Zettelkasten](https://en.wikipedia.org/wiki/Zettelkasten):

- "this is where we all came from": a git-based history of your notes
- "the dreams we have": an embedding of your notes 
- "the love we share": network of notes to graph
