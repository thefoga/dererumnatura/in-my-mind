# coding: utf-8

import argparse
import os
import numpy as np
from pathlib import Path
from datetime import datetime
from matplotlib import pyplot as plt
from collections import Counter

from src.common.io import get_config, get_folders
from src.common.times import pretty_dt, unix2datetime
from src.common.files import find_notes
from src.common.charts import get_figa, fig2file
from src.obsidian.notes import get_note_links, filter_urls

config = get_config('./config.ini')
CWD = Path(os.getcwd())


def get_current_note(root_notes, verbose=True):
    notes = list(find_notes(root_notes))

    if verbose:
        print('= {:d} notes'.format(len(notes)))

    return notes


def do_edit_distribution(root_notes):
    notes = get_current_note(root_notes)
    note_and_time = [
        (note, note.stat()[8])
        for note in notes
    ]
    note_and_time = list(sorted(note_and_time, key=lambda x: x[1]))

    fig, axis = get_figa(1, 1)

    note = note_and_time[0]  # oldest
    print('= oldest note "{}" last edited on {}'.format(
        note[0], pretty_dt(unix2datetime(note[1]), format='%m-%d %H:%M')
    ))

    note = note_and_time[-1]  # newest
    print('= newest note "{}" last edited on {}'.format(
        note[0], pretty_dt(unix2datetime(note[1]), format='%m-%d %H:%M')
    ))

    print('calculating last edit distribution ...')

    for note, modified_time in note_and_time:
        modified_time = unix2datetime(modified_time)

        axis.vlines(
            x=modified_time, ymin=0, ymax=1,
            linewidths=0.5, color='black', alpha=0.1
        )  # todo meaning of y axis?

    plt.xticks(rotation=50)
    out_folder = CWD / config.get('folders', 'out')
    fig2file(fig, out_folder / 'last_edit_distribution.png')


def classify_links(root_f, root_notes):
    all_alive, all_dead, all_external = [], [], []
    notes = get_current_note(root_notes)
    for note in notes:
        note_links = get_note_links(note, root_f)
        urls = [ text_and_url[1] for text_and_url in note_links ]

        alive, dead, external = filter_urls(urls)
        all_alive += alive
        all_dead += dead
        all_external += external

    make_uniques = lambda x: list(set(x))
    all_dead, all_alive, all_external = make_uniques(all_dead), make_uniques(all_alive), make_uniques(all_external)

    print('= links')
    print('  - {:d} externals'.format(len(all_external)))
    print('  - {:d} internals (alive)'.format(len(all_alive)))
    print('  - {:d} internals dead'.format(len(all_dead)))
    for link in all_dead:
        print('    @ {}'.format(link))


def do_link_distribution(root_f, root_notes):
    all_alive = []
    notes = get_current_note(root_notes)
    for note in notes:
        note_links = get_note_links(note, root_f)
        urls = [ text_and_url[1] for text_and_url in note_links ]

        alive, _, _ = filter_urls(urls)
        all_alive += alive

    all_alive = Counter(all_alive)

    hotspots = list(sorted(all_alive.items(), key=lambda x: x[1], reverse=True))[:15]
    print('= hotspots:')
    for note_and_inlinks in hotspots:
        print('  - {:3d} in-bound links: {}'.format(
            note_and_inlinks[1],
            note_and_inlinks[0]
        ))

    occurrences = list(all_alive.values())

    fig, axis = get_figa(1, 1)

    axis.hist(
        x=occurrences,
        bins=max(occurrences),
        color='black', alpha=1.0
    )
    axis.set_ylabel('occurrences')
    axis.set_xlabel('# in-links')

    plt.yscale('log', nonposy='clip')
    out_folder = CWD / config.get('folders', 'out')
    fig2file(fig, out_folder / 'links_distribution.png')

    # todo hotspot (mostly linked to): get_note_links


def get_edits(git_f, root_notes, date_time):
    # notes edited on ...: get_notes_edited_on
    # notes edited today: filter_edited_today
    pass  # todo


def suggest_linkage(root_notes):
    pass  # todo


def get_args():
    parser = argparse.ArgumentParser(usage='--action { count, anal, suggest }')

    parser.add_argument(
        '--action',
        dest='action',
        help='action to perform:\n' +
        '- edit_distrib: computes edit distribution (over time) of notes\n' +
        '- links: analyze links stats, check for dead ones\n' +
        '- edits: notes edited on ...\n' +
        '- suggest: suggest links\n',
        required=True
    )

    return parser.parse_args()


def main():
    root_f, root_notes, root_attach = get_folders(config)
    args = get_args()

    action = args.action

    if action == 'edits_distrib':
        do_edit_distribution(root_notes)
    elif action == 'links':
        classify_links(root_f, root_notes)
    elif action == 'links_distrib':
        do_link_distribution(root_f, root_notes)
    elif action == 'edits':
        date_time = datetime.now()  # todo add as param
        get_edits(root_f, root_notes, date_time)
    elif action == 'suggest':
        suggest_linkage(root_notes)
    else:
        print('Unrecognized action! Use --help')


if __name__ == '__main__':
    main()
