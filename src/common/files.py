def find_files(folder, extension):
    return folder.rglob('*' + extension)


def find_folders(folder):
    return sorted(filter(
        lambda p: p.is_dir(),
        folder.iterdir()
    ))


def find_notes(folder, extension='.md'):
    return find_files(folder, extension)
