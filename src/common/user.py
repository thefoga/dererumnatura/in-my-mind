def get_answer(question):
    print(str(question).strip())
    user_answer = input('')
    return user_answer.strip()


def get_yn_answer(question):
    user_answer = get_answer(str(question).strip() + ' [y/n]')
    return user_answer.startswith('y')


def get_int_answer(question):
    user_answer = get_answer(str(question).strip() + ' [int]')
    return int(user_answer)

