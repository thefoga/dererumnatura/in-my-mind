from pathlib import Path
from urllib.parse import unquote


def is_external_url(candidate):
    return candidate.startswith(('http', 'ftp', 'www'))


def fix_internal_url(url, root):
    return '{}/{}'.format(root, unquote(url))


def fix_url(url, root):
    if is_external_url(url):
        return unquote(url)

    original_candidate = fix_internal_url(url, root)

    if Path(original_candidate).exists():
        return original_candidate  # url is ok just as found (lucky case)

    # maybe url contains section: try to remove it
    section_i = original_candidate.rfind('#')
    if section_i > 0:
        candidate = original_candidate[:section_i]
    else:
        candidate = original_candidate

    if not candidate.endswith('.md'):
        candidate += '.md'

    if Path(candidate).exists():
        return candidate

    return original_candidate  # file not found, but alas, don't know how to fix it!