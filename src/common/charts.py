from matplotlib import pyplot as plt


def get_figsize(n_rows, n_cols):
    row_size = 12  # heigth
    column_size = 24  # width

    return (n_cols * column_size, n_rows * row_size)


def get_figa(n_rows=1, n_cols=1, figsize=None):
    if figsize is None:
        figsize = get_figsize(n_rows, n_cols)
    
    fig, ax = plt.subplots(n_rows, n_cols, figsize=figsize)
    return fig, ax


def fig2file(fig, f_out, verbose=True):
    plt.savefig(f_out, bbox_inches='tight', pad_inches=0.2, dpi=200)
    print('image saved to file://{}'.format(f_out))  # make it clickable
