from datetime import datetime


def now_dt():
    return datetime.now()


def pretty_dt(dt, format='%Y-%m-%d %H:%M:%S'):
    return dt.strftime(format)


def unix2datetime(x, millis=False):
    if millis:
        x /= 1000.0

    return datetime.fromtimestamp(x)


def same_time(x, y, comparisons=['day', 'month', 'year']):
    for attr in comparisons:
        if getattr(x, attr) != getattr(y, attr):
            return False

    return True