import pickle
from pathlib import Path
from configparser import ConfigParser


def get_config(file_path):
    config = ConfigParser()
    config.read(file_path)
    return config


def get_folders(config):
    root_f = Path(config.get('folders', 'zettelkasten_root')).expanduser()
    root_notes = root_f / config.get('folders', 'zettelkasten_notes')
    root_attach = root_f / config.get('folders', 'zettelkasten_attach')

    return root_f, root_notes, root_attach


def stuff2pickle(stuff, f_path):
    with open(f_path, 'wb') as fp:
        pickle.dump(stuff, fp)


def load_pickle(f_path):
    with open(f_path, 'rb') as fp:
        return pickle.load(fp)
