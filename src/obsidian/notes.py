import re
from pathlib import Path

from src.common.www import fix_url, is_external_url


def get_content(note_path):
    with open(note_path, 'r') as reader:
        return reader.read()


def find_markdown_links(
    content,
    root_folder,
    inline_link_re=re.compile(r'\[([^\]]+)\]\(([^)]+)\)'),
    footnote_link_text_re=re.compile(r'\[([^\]]+)\]\[(\d+)\]'),
    footnote_link_url_re=re.compile(r'\[(\d+)\]:\s+(\S+)')
    ):
    links = list(inline_link_re.findall(content))
    footnote_links = dict(footnote_link_text_re.findall(content))
    footnote_urls = dict(footnote_link_url_re.findall(content))

    for key in footnote_links.keys():
        links.append(
            (
                footnote_links[key], footnote_urls[footnote_links[key]]
            )
        )

    return map(
        lambda x: (x[0], fix_url(x[1], root_folder)),  # nice url
        links
    )  # (text, url)


def get_note_links(note_path, root_folder):
    content = get_content(note_path)
    return find_markdown_links(content, root_folder)


def filter_urls(urls):
    """ out: [ alive ], [ dead ], [ external ] """

    is_dead = lambda x: not is_external_url(x) and not Path(x).exists()
    is_alive = lambda x: not is_external_url(x) and Path(x).exists()
    is_external = lambda x: is_external_url(x)

    return filter(is_alive, urls), filter(is_dead, urls), filter(is_external, urls)