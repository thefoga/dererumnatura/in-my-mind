import re

from src.common.files import find_notes, find_files


def attach2file(attach_root):
    """ how Obsidian sees an attachment -> real filepath """

    def _f(x):
        x = x.replace('_attach/', '')
        x = x.replace('%20', ' ')
        return attach_root / x

    return _f


def file2attach(attach_root):
    """ how Obsidian sees an attachment """

    def _f(x):
        x = str(x).replace(str(attach_root), '')
        return x[1:]  # remove first /

    return _f


def find_attachments(file_path, attach_root):
    with open(file_path, 'r') as reader:
        content = reader.read()

    occs = re.findall('_attach\/[^)]*', content)  # todo generalize ()from attach_root), see https://regex101.com/
    return map(attach2file(attach_root), occs)


def find_all_attached(folder, attach_root, full_path=True):
    notes = find_notes(folder)
    all_attached = []

    for note_f in notes:
        all_attached += list(find_attachments(note_f, attach_root))

    all_attached = list(set(all_attached))  # rm duplicates

    if full_path:
        all_attached = list(map(file2attach(attach_root), all_attached))

    return list(set(all_attached))  # rm duplicates


def find_all_attachments(folder):
    extensions = ['.jpg', '.jpeg', '.png', '.gif', '.svg'] +\
        ['.mp4', '.ogg'] +\
        ['.pdf']
    extensions += list(map(lambda x: x.upper(), extensions))
    all_attachments = []

    for extension in extensions:  # todo very poor performance
        all_attachments += list(find_files(folder, extension))

    return list(set(all_attachments))  # rm duplicates
