from git import Repo, Commit
from datetime import datetime, timedelta

from src.common.times import unix2datetime, same_time


def filter_edited_today(notes):
    """ in: [ pathlib.Path ] """

    now = datetime.now()
    today_at_00 = datetime(now.year, now.month, now.day)
    tomorrow = now + timedelta(days=1)
    tomorrow_at_00 = datetime(tomorrow.year, tomorrow.month, tomorrow.day)

    note_and_time = [
        (note, unix2datetime(note.stat()[8]))
        for note in notes
    ]

    return filter(
        lambda x: today_at_00 <= x[1] < tomorrow_at_00,
        note_and_time
    )  # (note, datetime)


def get_notes_edited_on(notes, day, month, year, git_folder, branch='master'):
    day = datetime(day=day, month=month, year=year)

    repo = Repo(git_folder)
    for commit in repo.iter_commits(branch):
        if same_time(day, commit.committed_datetime):
            files = commit.stats.files
            for file in files:
                yield git_folder + file  # path
