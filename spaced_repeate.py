# coding: utf-8

import argparse
import numpy as np
from pathlib import Path
import copy
from datetime import datetime, timedelta

from src.common.io import get_config, get_folders, stuff2pickle, load_pickle
from src.common.times import now_dt, pretty_dt
from src.common.user import get_int_answer
from src.common.files import find_notes


config = get_config('./config.ini')
scores = {
    1: {
        'next reminder': 3,
        'description': 'I don\' remember it!'
    },
    2: {
        'next reminder': 10,
        'description': 'bad'
    },
    3: {
        'next reminder': 20,
        'description': 'so so'
    },
    4: {
        'next reminder': 30,
        'description': 'well'
    },
    5: {
        'next reminder': 40,
        'description': 'every part of it'
    }
}


def repeate_this(thing, memory):
    def _repeate_this():
        question = '- how well do you remember "{}" ?'.format(thing)
        return get_int_answer(question)

    memory[thing]['score'] = _repeate_this()
    memory[thing]['last time'] = now_dt()
    return memory[thing]


def print_stats(asked_things):
    print('asked {} things:'.format(len(asked_things)))

    print('{:>30}  {:>19}  {:>13}  {:>13}'.format(
        'thing', 'last asked on', 'last score', 'score today'
    ))
    print()

    for thing, info in asked_things.items():
        pre, post = info['pre'], info['post']

        print('{:>30}  {:>19}  {:>13d}  {:>13d}'.format(
            thing[-30:],
            pretty_dt(pre['last time'], format='%m-%d'),
            pre['score'],
            post['score']
        ))

    print()

    scores = [ info['pre']['score'] for info in asked_things.values() ]
    print('last time\'s scores ~ {:.3f} +- {:.3f}'.format(
        np.mean(scores), np.std(scores)
    ))

    scores = [ info['post']['score'] for info in asked_things.values() ]
    print('today\'s scores ~ {:.3f} +- {:.3f}'.format(
        np.mean(scores), np.std(scores)
    ))

    print()


def print_scoring():
    print('scoring:')

    for score, info in scores.items():
        print('- {}: {}, next reminder is in {} days'.format(
            score, info['description'], info['next reminder']
        ))

    print()


def do_repetitions(memory, n_max=10, force=False):
    print_scoring()
    asked_things = {}  # thing -> { pre, post }
    i_to_ask = np.random.randint(0, len(memory), size=n_max)  # todo fix order (priority, random ...)

    for to_ask in i_to_ask:
        thing, info = list(memory.items())[to_ask]

        if n_max and len(asked_things) < n_max:
            last_time = info['last time']
            last_score = info['score']

            reask_days = scores[last_score]['next reminder']
            reask_time = last_time + timedelta(days=reask_days)

            should_reask = now_dt() >= reask_time

            if should_reask or force:
                pre = copy.deepcopy(info)
                post = repeate_this(thing, memory)

                asked_things[thing] = {
                    'pre': pre,
                    'post': post
                }

    print()
    print_stats(asked_things)


def load_memory(f_path):
    f_path = Path(f_path).expanduser()

    if f_path.is_file():
        return load_pickle(f_path)

    return {}


def save_memory(memory, f_path):
    stuff2pickle(memory, Path(f_path).expanduser())


def do_scan(root_notes, memory, extension='.md'):
    for path in root_notes.rglob('*{}'.format(extension)):
        path = str(path)
        path = path.replace(str(root_notes), '')  # relative path

        if path not in memory:
            memory[path] = {
                'last time': datetime(1970, 1, 1),
                'score': 1
            }


def get_args():
    parser = argparse.ArgumentParser(usage='optional: --do_scan')

    parser.add_argument(
        '--do_scan',
        dest='do_scan',
        help='re-scans folder to see if there are new files, edits modification dates ...',
        required=False,
        default=True,
        action='store_true'
    )

    parser.add_argument(
        '--force',
        dest='force',
        help='forces to repeat even it it\'s not due',
        required=False,
        default=False,
        action='store_true'
    )

    return parser.parse_args()


def main():
    root_f, root_notes, root_attach = get_folders(config)
    db_file = '~/data/zettelkasten_spaced_repetitions.pkl'  # todo in config

    args = get_args()
    user_memory = load_memory(db_file)
    print('loaded {} things in memory'.format(len(user_memory)))

    if args.do_scan:
        memory_count = len(user_memory)
        do_scan(root_notes, user_memory)

        print('scan complete, added {} things to memory'.format(
            len(user_memory) - memory_count
        ))

    print()
    do_repetitions(user_memory, n_max=5, force=args.force)
    save_memory(user_memory, db_file)


if __name__ == '__main__':
    main()
